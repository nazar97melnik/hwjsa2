"use strict";
//Наведіть кілька прикладів, коли доречно використовувати в коді конструкцію `try...catch`.
//Як я зрозумів, її потрібно використовувати завжди, коли ми отримуємо данні із зовні (від користувача або з серверу приходить якийсь набір данинх), коли ми не впевнені , чи в коректному вигляді ми їх отримаємо, дана конструкція дає змогу продовжувати виконувати код, навіть коли в ньому є помилка, чи помилка в отриманні данних з серверу, без цієї конструкції наш код просто перестав би функціонувати.
// - Виведіть цей масив на екран у вигляді списку (тег ul – список має бути згенерований за допомогою Javascript).
// - На сторінці повинен знаходитись `div` з `id="root"`, куди і потрібно буде додати цей список (схоже завдання виконувалось в модулі basic).
// - Перед додаванням об'єкта на сторінку потрібно перевірити його на коректність (в об'єкті повинні міститися всі три властивості - author, name, price). Якщо якоїсь із цих властивостей немає, в консолі має висвітитися помилка із зазначенням - якої властивості немає в об'єкті.
// - Ті елементи масиву, які не є коректними за умовами попереднього пункту, не повинні з'явитися на сторінці.
const books = [
  {
    author: "Люсі Фолі",
    name: "Список запрошених",
    price: 70,
  },
  {
    author: "Сюзанна Кларк",
    name: "Джонатан Стрейндж і м-р Норрелл",
  },
  {
    name: "Дизайн. Книга для недизайнерів.",
    price: 70,
  },
  {
    author: "Алан Мур",
    name: "Неономікон",
    price: 70,
  },
  {
    author: "Террі Пратчетт",
    name: "Рухомі картинки",
    price: 40,
  },
  {
    author: "Анґус Гайленд",
    name: "Коти в мистецтві",
  },
];

const root = document.querySelector("#root");

//№1
// function checkBookProp(books) {
//   let requiredProp = ["author", "name", "price"];
//   let ul = document.createElement("ul");
//   try {
//     books.forEach((e) => {
//       if (requiredProp.every((value) => e.hasOwnProperty(value))) {
//         let li = document.createElement("li");
//         li.innerHTML = `<b>Назва</b> : ${e.name} <b>Автор</b> : ${e.author} <b>Ціна</b> : ${e.price}`;
//         ul.appendChild(li);
//       } else {
//         let missingProp = requiredProp.find((prop) => !e.hasOwnProperty(prop));
//         console.log(`Об'єкт не має значення ${missingProp}`);
//       }
//       root.append(ul);
//     });
//   } catch (e) {
//     console.log(e.message);
//   }
// }
// checkBookProp(books);

//№ 2
// // const root = document.querySelector("#root");
// function checkBookProp(books) {
//   let requiredProp = ["author", "name", "price"];
//   let ul = document.createElement("ul");
//   for (let obj of books) {
//     try {
//       if (requiredProp.every((prop) => obj.hasOwnProperty(prop))) {
//         const { name, author, price } = obj;
//         const li = document.createElement("li");
//         li.innerHTML = `<b>Назва книги</b> : ${name} <b>Ім'я автору</b> : ${author} <b>Ціна</b> : ${price}`;
//         ul.appendChild(li);
//         root.append(ul);
//       } else {
//         let missingProp = requiredProp.find(
//           (prop) => !obj.hasOwnProperty(prop)
//         );
//         throw new Error(`Об'єкт не має значення ${missingProp}`);
//       }
//     } catch (e) {
//       console.log(e.message);
//     }
//   }
// }
// checkBookProp(books);

//№ 3

// function checkBookProp(books, ...required) {
//   const list = document.createElement("ul");
//   books.forEach((e, i) => {
//     try {
//       required.forEach((prop) => {
//         if (!e[prop]) {
//           throw new Error(`Книга номер ${i + 1} немає ${prop}`);
//         }
//       });
//       const li = document.createElement("li");
//       li.innerHTML = `${e.name} ${e.author} - ${e.price}$`;
//       list.append(li);
//     } catch (e) {
//       console.error(e.message);
//     }
//   });
//   return list;
// }

// const booksList = checkBookProp(books, "author", "name", "price");
// root.append(booksList);
